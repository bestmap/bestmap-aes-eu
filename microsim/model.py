import sys
import pandas as pd

import farms
import attributes as att
import setup_farmers
import save_results




def run():
    farm_set = setup_farmers.setup_farmers()

    # Step 0: Update the world
    for farm in farm_set:
        farm.update_time_to_decide()

    for farm in farm_set:
        # Step 1: Check openness to each specific AES
        farm.check_opennness()
        #Step 2: Deliberation
        # Check for each farmer and AES whether offered-payment is high enough
        farm.deliberate_aes_decision()

    save_results.write_aes_records(farm_set)


if __name__ == '__main__':
    # Save results in specific location if it's an ensemble run or lambda test
    if len(sys.argv) == 2:
        save_results.set_location(sys.argv[1])
    run()
