import pandas as pd

import helper


country_info = pd.read_csv('../countries.csv')
countries = country_info['code'].to_list()
data_used = dict((country, country_info[country_info['code'] == country]['data_used'].item())
                 for country in countries)
hist_payments = dict((country, country_info[country_info['code'] == country]['payment'].item())
                 for country in countries)

"""
Parameters chosen by user
"""
# Load country and value of lambda by file.
# Note that these files are automatically changed
# when running run_ensembles.py or tune_lambda.py
country = helper.get_current_country()
lambda_open = helper.get_lambda_open()


prob_open_exp = 0  # probability the farmer is open based on previous experience
prob_open_adv = 1  # probability the farmer is open if they have access to an advisory
access_adv = 1  # probability the farmer has access to an advisory


# How to integrate regression results
NONE, YES_NO, SORTED = 0, 1, 2
MODEL_CHOICE_NAMES = ('none', 'yes_no', 'sorted')
model_choices = YES_NO

# Choose what payment farmers are offered. This may be the same as historic
# payments by setting offered_payment = hist_payments[country]
offered_payment = hist_payments[country] * 1.05

"""
Input file paths
"""

data_fp = '%s/%s%s.csv' % (helper.get_data_dir(), country, data_used[country])
mods_fp = 'inputs/mods/%s.csv' % country
openness_fp = 'inputs/openness/%s.csv' % country
total_farmers_fp = 'inputs/total_farmers/%s.csv' % country


"""
Constants
"""
# Standard deviation of WTA is assumed to be 10%
sd_mean_fraction = 0.1

ECO_SIZE = 'SE005'
HA_SIZE = 'SE025'
AES_PAYMENT = 'SAEAWSUB_V'


"""
Fixed according to data
"""
total_farmers = pd.read_csv(data_fp, usecols=('ID',)).shape[0]


"""
Automatically gained through clustering
"""
clusters = (7000, 30000, 100000)
