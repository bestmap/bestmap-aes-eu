"""  This module is used to produce an ensemble of runs from the model.

To run an ensemble for one country call
python run_ensembles.py SAMPLES CODE
where SAMPLES is the total times the model is to be run
and CODE is a country code as listed in the file countries.py (e.g. BEL)

To run an ensemble for each country call
python run_ensembles.py SAMPLES
where SAMPLES is the total times the model is to be run

Results are stored in results/ensemble_runs/[attributes.model_choices]/[country_code]
Files are named with a timestamp of the model run.
"""

import sys, os
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import attributes as att


def run_ensembles(samples, country_code=None):
    """Run the model for the total number of samples.
    If no country_code is specified, the model is run for all countries.
    """
    if isinstance(country_code, str):
        group = (country_code,)
    else:
        group = att.countries
    for country in group:
        print('Running ensemble for %s' % country)
        print(country, file=open('current_country', 'w'))
        for _ in range(samples):
            subprocess.run('python model.py ensemble', shell=True)


def plot_ensemble_size_variance(country_code):
    """Plot how ensemble variance is affected by the ensemble size."""
    mc = att.MODEL_CHOICE_NAMES[att.model_choices]
    timestamps = os.listdir('results/ensemble_runs/%s/%s' % (mc, country_code))
    preds = []
    for ts in timestamps:
        output = pd.read_csv('results/ensemble_runs/%s/%s/%s' % (mc, country_code, ts))
        preds.append(sum(output['open_to_aes'].to_list()))
    vars = []
    for i in range(3, len(timestamps)):
        vars.append(np.var(preds[:i]))
    plt.plot(range(3, len(timestamps)), vars)
    plt.show()



if __name__ == '__main__':
    if len(sys.argv) == 3:
        run_ensembles(int(sys.argv[1]), sys.argv[2])
    else:
        run_ensembles(int(sys.argv[1]))
