import csv
from datetime import datetime

import attributes as att


fp = 'results/%s/%s.csv' % (att.MODEL_CHOICE_NAMES[att.model_choices], att.country)


def set_location(location):
    """ Check where the results need to be stored.
    This depends on if it is a standard model run, part of an ensemble,
    or part of tuning lambda, as well as the country and model choice."""
    global fp
    if location == 'ensemble':
        timestamp = datetime.now().strftime("%y%m%d_%H%M%S_%f")
        fp_suffix = (att.MODEL_CHOICE_NAMES[att.model_choices],
                     att.country,
                     timestamp)
        fp = 'results/ensemble_runs/%s/%s/%s.csv' % fp_suffix
    elif location == 'lambda':
        fp_suffix = (att.MODEL_CHOICE_NAMES[att.model_choices],
                     str(round(att.lambda_open, 1))[-1],
                     att.country)
        fp = 'results/lambda_test/%s/%s/%s.csv' % fp_suffix


def write_aes_records(farms):
    """ Save the results."""
    with open(fp, 'w') as csvfile:
        aes_record = csv.writer(csvfile)
        aes_record.writerow(('aes_owner',
                             'open_to_aes',
                             'accepted_payment'
                             ))
        for farm in farms:
                aes_record.writerow((farm.id,
                                     farm.accepted_open,
                                     farm.accepted_payment
                                     ))
