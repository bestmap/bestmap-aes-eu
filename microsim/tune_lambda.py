""" This module helps to determine a good value of lambda_open.
lambda_open is multiplied by the proportion of farmers who adopt in the data
to estimate the proportion of farmers who are open to adopting an AES (but
might not adopt anyway, for example because the payment is insufficient).

The module runs the model for each country for a given value of lambda,
and returns the average model accuracy across all countries. Accuracy is a
value between 0 and 100, where 100 represents a perfect match with the data.
This is repeated for different values of lambda.
Results are printed to the terminal giving "lambda_open: accuracy"
"""


import subprocess
import pandas as pd
import numpy as np


import helper
import attributes as att



def run():
    """Run in each country for different values of lambda."""
    for lambda_open in np.linspace(1.0, 1.9, 10):
        lambda_open = round(lambda_open, 1)
        # save lambda and country details used by attributes.py
        print(lambda_open, file=open('lambda_open', 'w'))
        for country in att.countries:
            print(country, file=open('current_country', 'w'))
            subprocess.call('python model.py lambda', shell=True, stdout=subprocess.DEVNULL)


def get_actual_uptake(country):
    """ Get the list of which farms have adopted an AES from the data
    for the given country.
    """
    data_fp = '%s/%s%s.csv' % (helper.get_data_dir(), country, att.data_used[country])
    farmers = pd.read_csv(data_fp, usecols=('SAEAWSUB_V',))
    actual = farmers['SAEAWSUB_V'].to_list()
    for i in range(len(actual)):
        if actual[i] > 0:
            actual[i] = 1
    return actual


def get_microsim_prediction(country, lambda_open):
    """ Get the list of which farms have adopted an AES as predicted by the model
    for a given country and value of lambda_open
    """
    year = att.data_used[country]
    mc = att.MODEL_CHOICE_NAMES[att.model_choices]
    farmers = pd.read_csv('results/lambda_test/%s/%s/%s.csv' % (mc, lambda_open, country))
    microsim_predictions = farmers['open_to_aes'].to_list()
    return microsim_predictions


def get_microsim_accuracy(country, lambda_open):
    """ Get the accuracy of the model for a given country and value of lambda_open
    """
    actual = get_actual_uptake(country)
    prediction = get_microsim_prediction(country, lambda_open)
    n_correct = 0
    for i in range(len(actual)):
        if actual[i] == prediction[i]:
            n_correct += 1
    return n_correct / len(actual)


if __name__ == '__main__':
    run()
    print('lambda: average accuracy across all countries')
    n_countries = 28
    for lambda_open in np.linspace(1.0, 1.9, 10):
        lambda_open = str(round(lambda_open, 1))[-1]
        average_correct = 0
        for country in att.countries:
            accuracy = get_microsim_accuracy(country, lambda_open)
            average_correct += accuracy
        print('1.%s: %s' % (lambda_open, round(average_correct/n_countries, 3)))
