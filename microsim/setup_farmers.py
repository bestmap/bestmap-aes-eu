import pandas as pd
import numpy as np

from farms import farm
import helper
import attributes as att
import wta


N_FSA_TYPES = 5
N_ECO_SIZE_CLUSTERS = 3
SMALL, MEDIUM, LARGE = range(N_ECO_SIZE_CLUSTERS)
intrinsic_openness = pd.read_csv(att.openness_fp, header=None).to_numpy()


def _fsa(row):
    """Get the FSA group of the farmer from the csv row of data."""
    TF = int(row['TF8'])
    if TF == 1:
        return 1  # general cropping
    elif TF == 2:
        return 2  # horticulture
    elif TF == 3 or TF == 4:
        return 3  # permanent crops
    elif 5 <= TF <= 7:
        return 4  # livestock
    return 5  # mixed



def _eco_size(row):
    """Get the economic size (small, medium, large) from the csv row of data."""
    THRESH_SMALL, THRESH_MEDIUM, THRESH_LARGE = att.clusters
    farm_size = row[att.ECO_SIZE]
    if farm_size <= THRESH_SMALL:
        return SMALL
    elif farm_size <= THRESH_MEDIUM:
        return MEDIUM
    else:
        return LARGE


def _intrinsic_opennenss(row):
    """Get the intrinsic openness of the farmer using the csv row of data.
    Opennness is subject to FSA and economic size.
    """
    farm_size = float(row[att.ECO_SIZE])  # economic size
    THRESH_SMALL, THRESH_MEDIUM, THRESH_LARGE = att.clusters
    if farm_size <= THRESH_SMALL:
        return intrinsic_openness[(N_FSA_TYPES * 2) + _fsa(row) - 1][0]
    if farm_size <= THRESH_MEDIUM:
        return intrinsic_openness[N_FSA_TYPES + _fsa(row) - 1][0]
    else:  # large
        return intrinsic_openness[_fsa(row) - 1][0]


def _accepted_payment(row):
    """Choose the accepted payment for the farmer.
    Based on the setting chosen in attributes.model_choices.
    """
    fsa = _fsa(row) - 1
    eco_size = _eco_size(row)
    apl = wta.wta[fsa][eco_size]  # initial accepted payment list
    # Standard deviation is set by att.sd_mean_fraction
    sd = apl * att.sd_mean_fraction
    if att.model_choices == att.YES_NO:
        prob_update_list = row['mod']
        if apl == np.inf or sd < 0:
            # For when there is only one country in the FSA group
            apl = att.hist_payments[att.country]
            sd = apl * att.sd_mean_fraction
        apl = helper.random_normal_controlled(mean=apl,
                                              sd=sd,
                                              factor=prob_update_list)
    elif att.model_choices == att.NONE:
        if apl == np.inf or sd < 0:
            # For when there is only one country in the FSA group
            apl = att.hist_payments[att.country]
        else:
            apl = np.random.normal(apl, sd)
    elif att.model_choices == att.SORTED:
        p = int(row['subset_mod_rank'] - 1)
        apl = wta.norm_combined_lists[fsa][eco_size][p]
    # replace negative WTA with 0
    if apl < 0:
        apl = 0
    else:
        apl = round(apl)
    return apl


def setup_farmers():
    """Create a farm object for all farmers."""
    wta.set_wta()
    farmers = pd.read_csv(att.data_fp)
    mods = pd.read_csv(att.mods_fp)
    farmers['mod'] = mods['mod']
    farmers['subset_mod_rank'] = mods['subset_mod_rank']
    farms = np.empty(att.total_farmers, dtype=object)
    for index, row in farmers.iterrows():
        fsa = _fsa(row) - 1
        eco_size = _eco_size(row)
        # Get intrinsic opennness
        openness = _intrinsic_opennenss(row)
        apl = _accepted_payment(row)
        # No longer used in the model but to be reintroduced if multiple ticks are used.
        # if row['SAEAWSUB_V'] > 0:
        #     prior_exp = 1
        # else:
        #     prior_exp = 0
        f = farm(id = row['ID'],
                 area = row['SE025'],
                 openness=openness,
                 accepted_payment=apl,
                 offered_payment=att.offered_payment)
        # while it seems unnecessary to have offered payment per farmer
        # given it's the same value for every farm, I'm keeping it as a farm attribute
        # because I might change the code so it differs depending of farm attributes (e.g. FSA)
        farms[index] = f
    return farms
