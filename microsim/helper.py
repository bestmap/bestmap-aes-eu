from numpy import random, empty, inf


def list_random_higher_than_mean(mean, sd, len_list):
    """Return a list of values higher than the given normal distribution."""
    if len_list == 0:
        return []
    if mean == inf:
        return [9999999 for i in range(len_list)]
    result_list = empty(len_list)
    for i in range(len_list):
        result = random.normal(mean, sd)
        while result < mean:
            result = random.normal(mean, sd)
        result_list[i] = result
    return [round(x, 2) for x in result_list]


def list_random_lower_than_mean(mean, sd, len_list):
    """Return a list of values lower than the given normal distribution."""
    if len_list == 0:
        return []
    if mean == -inf:
        return [9999999 for i in range(len_list)]
    result_list = empty(len_list)
    for i in range(len_list):
        result = random.normal(mean, sd)
        while result > mean or result < 0:
            result = random.normal(mean, sd)
        result_list[i] = result
    return [round(x, 2) for x in result_list]


def get_random_1_with_p(p):
    """ Return value of 1 with probability of p. """
    if random.random() < p:
        return 1
    return 0


def random_normal_controlled(mean, sd, factor):
    """Return a single value higher or lower than then mean of the normal distribution.
    If factor > 0.5 return value larger than mean,
    else return value lower.
    """
    result = random.normal(mean, sd)
    if factor > 0.5:
        while result > mean:
            result = random.normal(mean, sd)
    else:
        while result < mean:
            result = random.normal(mean, sd)
    return result


def _clean_string(s):
    """Remove any superfluous new line from the string."""
    try:
        return s[:s.index('\n')]
    except ValueError:
        return s


def get_data_dir():
    """ Retrieve the filepath of the data from the text file."""
    with open('../data_fp', 'r') as f:
        data_fp = f.readline()
    return _clean_string(data_fp)


def get_current_country():
    """ Retrieve the current country to be simulated."""
    with open('current_country', 'r') as f:
        country = f.readline()
    return _clean_string(country)


def get_lambda_open():
    """ Retrieve the value of lambda_open to be used to determine opennness."""
    with open('lambda_open', 'r') as f:
        lambda_open = f.readline()
    return float(_clean_string(lambda_open))
