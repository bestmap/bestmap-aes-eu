""" Run the model for once for each country
"""

import subprocess

import attributes as att


for country in att.countries:
    print('Running for %s' % country)
    print(country, file=open('current_country', 'w'))
    subprocess.run('python model.py', shell=True)
