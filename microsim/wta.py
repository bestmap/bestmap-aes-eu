"""
This module is used during the setup of farmers to calculate their accepted payments.
If using model_choices=NONE then variable wta determines accepted payments.
if model_choices=SORTED then the variable norm_combined_lists is used.
"""


from scipy import stats
import pandas as pd
import numpy as np

import attributes as att
import helper


N_FSA_TYPES = 5
N_ECO_SIZE_CLUSTERS = 3

thresholds = np.empty((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
total_farmers = np.empty((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
hist_payments = np.zeros((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
wta = np.zeros((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
norm_combined_lists = np.empty((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS), dtype=list)


def set_wta():
    """ Get thresholds, total farmers and offered payments per group. """
    # Threshold and intrinsic openness are currently set the same
    intrinsic_openness_df = pd.read_csv(att.openness_fp, header=None).to_numpy()
    total_farmers_df = pd.read_csv(att.total_farmers_fp, header=None).to_numpy()
    i = 0
    for eco_size in range(N_ECO_SIZE_CLUSTERS-1, -1, -1):  # stored in reverse order of size
        for fsa in range(N_FSA_TYPES):
            thresholds[fsa][eco_size] = intrinsic_openness_df[i][0]
            total_farmers[fsa][eco_size] = total_farmers_df[i][0]
            hist_payments[fsa][eco_size] = att.hist_payments[att.country]
            i += 1


    """ Get wta """
    for eco_size in range(N_ECO_SIZE_CLUSTERS):
        for fsa in range(N_FSA_TYPES):
            perc_adopted = thresholds[fsa][eco_size]
            dist_from_mean = stats.norm.ppf(perc_adopted)
            wta[fsa][eco_size] = hist_payments[fsa][eco_size] - (dist_from_mean * att.sd_mean_fraction)


    """ Get accepted payments to be given to the farmers of each group is using model_choices=SORTED. """
    for eco_size in range(N_ECO_SIZE_CLUSTERS):
        for fsa in range(N_FSA_TYPES):
            if total_farmers[fsa][eco_size] > 0:
                p = wta[fsa][eco_size]
                sd_value = att.sd_mean_fraction * p
                # people who won't adopt
                if thresholds[fsa][eco_size] * att.lambda_open >= 1:
                    len_list = 0
                else:
                    len_list = round(total_farmers[fsa][eco_size] * (1 - (thresholds[fsa][eco_size] * att.lambda_open)))
                #print(eco_size, fsa, len_list)
                lower_len_list = int(total_farmers[fsa][eco_size] - len_list)  # people who will adopt
                high_list = sorted(helper.list_random_higher_than_mean(p, sd_value, len_list))
                low_list = sorted(helper.list_random_lower_than_mean(p, sd_value, lower_len_list))
                if len(high_list) > 0:
                    if np.isinf(high_list[0]) or np.isnan(high_list[0]):
                        high_list = [999999 for i in range(len(high_list))]
                combined_list = low_list[:]
                combined_list.extend(high_list)
                norm_combined_lists[fsa][eco_size] = combined_list
