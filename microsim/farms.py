import numpy as np

import helper
import attributes as att


class farm:
    def __init__(self, id, area, openness, accepted_payment, offered_payment):
        # Variables straight from CSV file
        self.id = id
        self.area = area
        # Variables set by model parameters
        self.openness = openness  # are they intrinsically open
        self.accepted_payment = accepted_payment  # payment the farmer wants
        self.offered_payment = offered_payment  # payment the farmer is offered
        # Other
        self.open_to_aes = 0  # probability the farmer is open to adoption
        self.time_to_decide = True
        #Does the farmer have access to an advisory?
        self.p_advisory = helper.get_random_1_with_p(att.access_adv)
        self.accepted_open = False  # Has the farmer decided to adopt an AES
        # Currently unused but useful if the model is updated to have multiple ticks.
        self.contract = None


    def check_opennness(self):
        """ Check if the farmer is open to adopting an AES.
        Based on previous experiene, intrinsic openness, and access to an advisory.
        """
        self.open_to_aes = 0
        x = helper.get_random_1_with_p(att.prob_open_exp)
        if x == 0:
            x = helper.get_random_1_with_p(self.openness)
        if x == 0 and self.p_advisory == 1:
            x = helper.get_random_1_with_p(att.prob_open_adv)
        self.open_to_aes = x


    def update_time_to_decide(self):
        """ Check if the farmer doesn't have a contract and is therefore
        ready to decide on adopting a new AES.
        """
        # Currently unused but will be of use when introducing multiple ticks.
        if self.contract is None:
            self.time_to_decide = True
            return
        self.time_to_decide = False


    def deliberate_aes_decision(self):
        """ Decide whether to adopt an AES.
        Based on if the farmer is open and if the payment is acceptable.
        """
        accepted_aes = 0
        if self.time_to_decide:
            # compare offered payment to accepted payment
            if self.offered_payment >= self.accepted_payment:
                accepted_aes = True
            self.accepted_open = accepted_aes * self.open_to_aes
