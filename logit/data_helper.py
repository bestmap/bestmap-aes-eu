""" This file is used to upload the data,
    remove unnecessary variables
    and deal with categorical variables.
"""

import random
import numpy as np
import pandas as pd
from scipy.stats import mode


with open('../data_fp', 'r') as f:
    data_fp = f.readline()
    try:
        # Remove any superfluous new line
        data_fp = data_fp[:data_fp.index('\n')]
    except ValueError:
        pass


country_info = pd.read_csv('../countries.csv')
countries = country_info['code'].to_list()
data_used = dict((country, str(country_info[country_info['code'] == country]['data_used'].item()))
                 for country in countries)
names = dict((country, country_info[country_info['code'] == country]['name'].item())
                 for country in countries)

variables = pd.read_csv('FADN_data_variables.csv')


def split_categories(column):
    """Split categorical variables into separate binary details for all but one category."""
    unique_values = np.unique(column)
    if len(unique_values) == 1:
        raise ValueError("Only one value in column. Splitting is not necessary.")
    common_value = mode(column).mode[0]
    unique_values = np.delete(unique_values, np.where(unique_values==common_value))
    binary_split = np.zeros((len(column), len(unique_values)), dtype=int)
    for data_i in range(len(column)):
        if column[data_i] != common_value:
            cat_idx = np.where(unique_values==column[data_i])[0][0]
            binary_split[data_i][cat_idx] = 1
    return binary_split, unique_values


def _load_data(country, years):
    """Return unchanged data of the country for either a single year or multiple.
    year must be a string (e.g. '2014') or a group of strings (e.g. ('2014', 2015')).
    """
    if isinstance(years, str):
        X = pd.read_csv('%s/%s%s.csv' % (data_fp, country, years))
    else:
        X = pd.DataFrame()
        for year in years:
            df = pd.read_csv('%s/%s%s.csv' % (data_fp, country, year))
            X = pd.concat([X, df], ignore_index=True)
    return X


def get_y_data(country, years):
    """Return the dependent varaible of the country for the given years."""
    X = _load_data(country, years)
    payments = X['SAEAWSUB_V'].to_numpy()
    Y = np.zeros(X.shape[0], dtype=bool)
    for i in range(X.shape[0]):
        if payments[i] > 0:
            Y[i] = True
    return Y


def get_data(country, years, include_ID=False):
    """Returns (independent, dependent) data."""
    # Find out which columns to get for the independent data
    X = _load_data(country, years)
    payments = X['SAEAWSUB_V'].to_numpy()  # output variable

    # Drop the dependent variable from the independent data and related data
    to_drop = ['SAEAWSUB_V', 'SE621', 'SAEAWSUB_2_V', 'SAEAWSUB_3_V', 'SE624']
    # Drop the non-numerical columns and others not needed
    # Dropping altitude as it often has missing data
    to_drop.extend(['COUNTRY', 'SEX', 'YEAR', 'countryyear', 'ALTITUDE'])
    if not include_ID:
        # NOTE: ID needs to be dropped for training but kept for prediction
        to_drop.extend(['ID'])
    # Drop columns related to subsidies or other payments received
    for index, row in variables.iterrows():
        if ('subsidy' in row['description'] or
                'Subsidy' in row['description'] or
                'subsidies' in row['description'] or
                'Subsidies' in row['description'] or
                'payment' in row['description'] or
                'Payment' in row['description']):
            to_drop.append(row['variable'])
    X = X.drop(columns=to_drop)

    #Remove columns where every value is the same
    to_drop = []
    for var in X.columns:
        if min(X[var]) == max(X[var]):
            to_drop.append(var)
    X = X.drop(columns=to_drop)

    # Split categorical data.
    # First find out which variables correspond to categories
    for index, row in variables.iterrows():
        cat_var = row['variable']
        try:
            if (cat_var[-2:] == '_T' or
                    'type' in row['description'] or
                    'Type' in row['description'] or
                    'class' in row['description'] or
                    'Class' in row['description'] or
                    'Clustered' in row['description'] or
                    'Region' in row['description'] or
                    'NUTS3' in row['description']):
                cat_data, cat_types = split_categories(X[cat_var])
                new_cat_cols = ['%s_%s' % (cat_var, str(type)) for type in cat_types]
                df = pd.DataFrame(cat_data, columns=new_cat_cols)
                X = X.drop(columns=[cat_var])
                X = pd.concat([X, df], axis=1)
        except (KeyError, ValueError):
            # This variable has already been removed because all values were the same.
            pass
    # NOTE: there's some potentially unnecessary overlap in categories
    # e.g. economic size is split into 6 categories (SIZ6) and into 14 (SIZC)

    # Change the dependent variable to a boolean list
    # Alter value of payment to a boolean of whether the farm received a payment
    Y = np.zeros(X.shape[0], dtype=bool)
    for i in range(X.shape[0]):
        if payments[i] > 0:
            Y[i] = True
    return X, Y


def get_data_even_split(country, years, include_ID=False):
    """ Return a subset of the data such that there is an even split
        of farms who do and don't take up a scheme.
    """
    # Use fixed seed so we retrieve the same data during analysis.
    random.seed(20)
    X, Y = get_data(country, years, include_ID)
    new_X, new_Y = pd.DataFrame(), pd.DataFrame()
    pos = [i for i in range(len(Y)) if Y[i] == True]
    neg = [i for i in range(len(Y)) if Y[i] == False]
    if len(pos) < len(neg):
        fullset = pos
        subset = neg
    else:
        fullset = neg
        subset = pos
    new_X = X.iloc[fullset]
    new_Y = [Y[i] for i in fullset]
    sample = random.sample(subset, len(fullset))
    new_X = pd.concat([new_X, X.iloc[sample]], ignore_index=True)
    new_Y.extend([Y[i] for i in sample])
    return new_X, np.array(new_Y)


def get_y_data_even_split(country, years):
    """ Return a subset of the dependent variable from the data
        such that there is an even split of farms who do and don't
        take up a scheme.
    """
    X, y = get_data_even_split(country, years)
    return y
