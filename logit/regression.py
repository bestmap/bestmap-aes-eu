""" This file is used find to find variables for each country for a GLM.
    Forward, backward, or stepwise regression can be performed.
"""


import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import warnings
import pandas as pd
from collections import defaultdict
import csv


import data_helper


# Variables set via update_data()
X, y = None, None
country_variables = []  # the set of variables used for that country in case not all are used
year = None

variables = pd.read_csv('FADN_data_variables.csv')
variables = defaultdict(str, ((row['variable'], row['description']) for index, row in variables.iterrows()))


"""
Private functions
"""
def _run_model(vars):
    """ Returns SST and pvalue of each parameter in vars."""
    x_train = X[vars].copy()
    model = sm.GLM(y, x_train, family=sm.families.Binomial())
    res = model.fit()
    sse = np.sum((res.fittedvalues - y)**2)
    ssr = np.sum((res.fittedvalues - y.mean())**2)
    sst = ssr + sse
    return(round(sst), res.pvalues.to_dict())


def _add_variable(fixed_vars, new_var):
    """ Test adding a variable to the model.
        fixed_vars is a list of variables already included.
        new_var is the new variable being tested.
    """
    vars = fixed_vars[:]
    vars.append(new_var)
    sst, pvalues = _run_model(vars)
    return sst, pvalues[new_var]


def _remove_variable(full_set, var_to_remove):
    """ Test removeing a variable from the model.
        full_set is a list of variables already included.
        var_to_remove is the new variable to be removed from full_set
    """
    vars = full_set[:]
    vars.remove(var_to_remove)
    sst, pvalues = _run_model(vars)
    return sst


def _forward_selection_step(vars_in_model, vars_to_select):
    """ Return the new variable to add to the model.
    Return None if no variables met the criteria.
    """
    lowest_sst = np.inf
    p_of_best = None
    best_var = None
    for new_var in vars_to_select:
        try:
            sst, p = _add_variable(vars_in_model, new_var)
            if sst < lowest_sst:
                lowest_sst = sst
                p_of_best = p
                best_var = new_var
        except RuntimeWarning:
            pass  # this variables causes division by zero
    if p_of_best >= 0.1:
        return None
    return best_var


def _backward_elimination_step(vars_in_model):
    """ Return the variable to be eliminated from the model.
    Return None if no variables meet the criteria.
    """
    sst_full, pvalues_full = _run_model(vars_in_model)
    highest_sst = -np.inf
    worst_var = None
    vars_to_consider = [var for var, p in pvalues_full.items() if p > 0.2]
    for var in vars_to_consider:
        sst = _remove_variable(vars_in_model, var)
        if sst > highest_sst:
            highest_sst = sst
            worst_var = var
    return worst_var


"""
Functions to be run by the user.
"""
""" Always run this first to set the country for regression."""
def update_data(country):
    # This function exists so if I change which data I use (all or even split)
    # or which parameters I use (all or a subset)
    # then the change is set for all regression countries in a batch run.
    global X
    global y
    global country_variables
    global year
    year = data_helper.data_used[country]
    X, y = data_helper.get_data_even_split(country, year)
    country_variables = X.columns.to_list()  # use default of all columns


def forward_selection():
    """ Run forward selection regression.
    Returns a list of the variables added.
    """
    warnings.filterwarnings('error')
    vars_in_model = []
    vars_to_select = country_variables
    while True:
        new_var = _forward_selection_step(vars_in_model, vars_to_select)
        if new_var == None:
            return vars_in_model
        vars_in_model.append(new_var)
        vars_to_select = np.delete(vars_to_select, np.where(vars_to_select==new_var))
    warnings.resetwarnings()
    return vars_in_model


def backward_elimination():
    """ Run backward selection regression.
    Returns a list of the variables remaining.
    """
    vars_in_model = country_variables
    while True:
        elim_var = _backward_elimination_step(vars_in_model)
        if elim_var == None:
            return vars_in_model
        print('Removing', elim_var)
        vars_in_model.remove(elim_var)


def stepwise_regression(verbose=False):
    """ Run stepwise regression.
    Returns a list of variables added to the model.
    """
    # Turn warnings to errors otherwise non-sensical variables get added.
    warnings.filterwarnings('error')
    vars_in_model = []
    vars_to_select = country_variables
    new_var = _forward_selection_step(vars_in_model, vars_to_select)
    if new_var == None:
        raise Exception('Nothing has been added.')
    else:
        vars_in_model.append(new_var)
        vars_to_select.remove(new_var)
        if verbose: print('adding', new_var, variables[new_var])
    time_to_stop = False
    # Keep going through the model alternating between
    # seeing if there's a variable to add, and
    # seeing if there's a variable to remove.
    # If nothing changes then we're done.
    while not time_to_stop:
        something_changed = False
        new_var = _forward_selection_step(vars_in_model, vars_to_select)
        if new_var != None:
            vars_in_model.append(new_var)
            vars_to_select.remove(new_var)
            something_changed = True
            if verbose: print('adding', new_var, variables[new_var])
        if len(vars_in_model) > 1:  # can't remove our only variable
            elim_var = _backward_elimination_step(vars_in_model)
            if elim_var != None:
                vars_in_model.remove(elim_var)
                vars_to_select.append(elim_var)
                something_changed = True
                if verbose: print('removing', elim_var, variables[new_var])
        if not something_changed:
            time_to_stop = True
    warnings.resetwarnings()
    return vars_in_model


if __name__ == '__main__':
    # Can use forward_selection() or backward_elimination() instead of stepwise_regression()
    for country in data_helper.countries:
        update_data(country)
        try:
            vars = stepwise_regression(verbose=False)
            print(country, vars)
        except:
            print(country, 'nothing added')
