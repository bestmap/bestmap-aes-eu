""" This file stores the results of the parameters selected by stepwise regression.

    Data from the year 2014 was used, unless otherwise specified in a comment.
"""


# Using all of the data

all_data = {
    'BEL': ['SE495', 'SE054', 'SE206'],
    'ESP': ['WUCCA_P', 'SE136', 'IMAN_V', 'SE005', 'REGION_545', 'SE165', 'NUTS3_ES220', 'ANC3_3'],
    'LTU': ['OGASHARE', 'AASBIO_CV'],
    'POR': ['ORGNA', 'SE285', 'AGE', 'AINVNTS_CV', 'SYS02'],
    'BGR': ['WPCCA_P', 'SE425', 'SE495', 'SE136'],
    'EST': ['ORGANIC', 'SE025', 'SE015'],
    'LUX': ['AGE'],
    'ROU': ['OGASHARE'],
    'CYP': ['OGASHARE', 'SE065'],
    'FRA': ['SE132', 'SE284', 'SE110', 'TF8_7', 'SE074', 'SE425', 'REGION_163', 'SE532', 'ILNDRNT_V', 'SE285', 'SE170'],
    'LVA': ['AGE', 'SE120', 'SE430', 'SE136', 'SYS02'],
    'SUO': ['SE025'],  # after removing warnings
    'CZE': ['SE120D', 'AASBIO_CV', 'SE085', 'CVEGOF_TA', 'SE055', 'SIZ6_2', 'SE050', 'ALNDFRSTST_CV', 'ALNDIMP_CV', 'ANC3_2'],
    'HRV': ['WPCCA_P'],
    'MLT': ['OGASHARE', 'SE185'],
    'SVE': ['TF', 'SE120', 'SE025', 'SE005', 'SIZ6_4'],
    'DAN': ['NAT2000', 'ORGANIC', 'SE125'],
    'HUN': ['SE132', 'SYS02', 'AASBIO_CV', 'SE284', 'AINVNTS_CV', 'SE022'],
    'NED': ['NAT2000', 'SE136', 'SE320', 'AASBIO_CV', 'ACSHEQ_CV', 'SE251', 'SE298'],
    'SVK': ['SE015', 'SE132', 'SE284', 'SE136'],
    'DEU': ['SE132', 'SE072', 'ALNDFRSTST_CV', 'SE170', 'WPROTH_P', 'SE430', 'SE200', 'NUTS3_DE939', 'AGE', 'SE155'],
    'IRE': ['SE284', 'SE120'],
    'OST': ['ORGANIC', 'SE025', 'ALNDFRSTST_CV'],
    'SVN': ['AGE', 'ALNDFRSTST_CV', 'SE010', 'SE140', 'AASBIO_CV'],
    'ELL': ['OGASHARE', 'WUCCA_P', 'NUTS3_EL612', 'ALNDIMP_CV', 'SE132', 'WPCCA_P', 'SYS02'],
    'ITA': ['WPCCA_P', 'SE120D', 'SE080', 'SE284', 'NUTS3_ITH10', 'SE165', 'REGION_312', 'ALNDAGR_CV', 'REGION_302', 'NUTS3_ITI43'],
    'POL': ['WPCCA_P', 'ORGANIC', 'SE145', 'SE175', 'SE132', 'IINS_V', 'SE054', 'SIZ6_2'],
    'UKI': ['SSPSN_1_V', 'REGION_413', 'REGION_411', 'SE095', 'SE022', 'SE110', 'SE300']
    }


# Using a 50/50 split of AES uptake in the data
split_data = {
    'BEL': ['SE495', 'SE206', 'SE136', 'SE055'],
    'ESP': ['SE165', 'SE054', 'ACSHEQ_CV', 'SE074', 'SE410', 'AASBIO_CV', 'SE136', 'SE175', 'ORGANIC', 'SE131', 'SE281', 'SE125', 'SE750', 'SE011', 'SE356', 'SSPSN_1_V', 'ICNTR_V', 'SSPSS_1_V', 'AMCHQP_CV', 'UAAOWNED'],
    'LTU': ['AASBIO_CV'],
    'POR': ['AASBIO_CV', 'SE046'],
    'BGR': ['SE495', 'SYS02', 'SE425', 'SE132', 'TF8_6', 'TF', 'NUTS3_BG332'],
    'EST': ['SE071', 'SE132', 'UAAOWNED', 'ORGANIC'],
    'LUX': ['AGE'],  # nothing added
    'ROU': ['SE410', 'OGASHARE'],
    'CYP': ['SE065'],
    'FRA': ['SE284', 'NAT2000', 'NUTS3_FRA10', 'SE305', 'SE441', 'SE309N', 'OTRISM_SV', 'NUTS3_FRA40', 'NUTS3_FR432'],
    'LVA': ['SE120', 'SE005'],
    'SUO': ['SE046', 'SE501'],
    'CZE': ['SE460', 'SE110', 'SE225', 'AASBIO_CV', 'ACSHEQ_CV'],
    'HRV': ['SE100', 'AASBIO_SV'],
    'MLT': ['AASBIO_CV', 'AASBIO_SV'],
    'SVE': ['SE120', 'SE132'],
    'DAN': ['SE650', 'AGE', 'SE074', 'SE072'],
    'HUN': ['SE330', 'AASBIO_CV', 'WPCCA_P', 'SE265', 'ORGANIC', 'SE125N', 'TYPOWN_2'],
    'NED': ['SE005', 'SE025', 'SE298', 'SIZC_14'],
    'SVK': ['SE080', 'SE110'],
    'DEU': ['SE072', 'ALNDFRSTST_CV', 'SE170', 'WPROTH_P', 'SE095', 'ORGANIC', 'WPCCA_P', 'SE185', 'NUTS3_DE118', 'SE200', 'NUTS3_DE21', 'NUTS3_DE937', 'NUTS3_DE939', 'NUTS3_DEB23'],
    'IRE': ['ACSHEQ_CV', 'ANC'],
    'OST': ['SE284', 'ALNDAGR_CV', 'SE185', 'ORGANIC', 'SE072', 'AASBIO_SV'],
    'SVN': ['SE041', 'ALNDAGR_CV', 'SE284N'],
    'ELL': ['ACSHEQ_CV', 'ORGNA', 'AASBIO_CV', 'NUTS3_EL543'],
    'ITA': ['SE120D', 'SE132', 'SE054', 'SE050', 'SE165', 'AGE', 'SE206', 'AFRMBLD_CV', 'SE041', 'SE305'],
    'POL': ['SE145', 'SE132', 'SE425', 'ORGANIC', 'WPCCA_P', 'AASBIO_CV', 'SE095', 'SE105', 'SE506'],
    'UKI': ['SE025', 'SE011', 'STRUCTF', 'SE005']
    }



data_used = {
    'BEL': '2014',
    'ESP': '2014',
    'LTU': '2014',
    'POR': '2014',
    'BGR': '2014',
    'EST': '2014',
    'LUX': '2014',
    'ROU': '2016',
    'CYP': '2015',
    'FRA': '2014',
    'LVA': '2015',
    'SUO': '2014',
    'CZE': '2014',
    'HRV': '2014',
    'MLT': '2014',
    'SVE': '2014',
    'DAN': '2014',
    'HUN': '2014',
    'NED': '2015',  # there's an error in the 2014 data
    'SVK': '2014',
    'DEU': '2014',
    'IRE': '2015',
    'OST': '2015',
    'SVN': '2015',
    'ELL': '2014',
    'ITA': '2014',
    'POL': '2014',
    'UKI': '2014'
    }
