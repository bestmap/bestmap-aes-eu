""" This file is used to generate predictions using the GLM. """


import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import pandas as pd


import regression
import data_helper
import results


variables = pd.read_csv('FADN_data_variables.csv')


def plot_results_grid(y_test, y_pred):
    """Plot the confusion matrix of the predicted results."""
    true_pos = 0
    true_neg = 0
    false_pos = 0
    false_neg = 0
    for i in range(len(y_pred)):
        if y_pred[i] < 0.5 and y_test[i] == 0:
            true_neg += 1
        elif y_pred[i] < 0.5 and y_test[i] == 1:
            false_neg += 1
        elif y_pred[i] >= 0.5 and y_test[i] == 0:
            false_pos += 1
        elif y_pred[i] >= 0.5 and y_test[i] == 1:
            true_pos += 1

    confusion_matrix = ((true_neg/(true_neg+false_pos), false_pos/(true_neg+false_pos)),
                        (false_neg/(false_neg+true_pos), true_pos/(false_neg+true_pos)))
    precision_cm = ((true_neg, false_pos),
                    (false_neg, true_pos))
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.imshow(precision_cm)
    ax.grid(False)
    ax.xaxis.set(ticks=(0, 1), ticklabels=('Predicted 0s', 'Predicted 1s'))
    ax.yaxis.set(ticks=(0, 1), ticklabels=('Actual 0s', 'Actual 1s'))
    ax.set_ylim(1.5, -0.5)
    for i in range(2):
        for j in range(2):
            ax.text(j, i, '%.2f \n (%.2f)' % (precision_cm[i][j], confusion_matrix[i][j]), ha='center', va='center', color='red')
    plt.show()


def print_brief_summary(model):
    """Print details of parameters, their descriptions and coefficients."""
    for param in model.params.keys():
        coef = model.params[param]
        try:
            desc = variables[variables['variable']==param]['description'].item()
        except ValueError:
            p1 = param[:param.index('_')]
            p2 = param[param.index('_')+1:]
            p1_desc = variables[variables['variable']==p1]['description'].item()
            desc = '%s: class %s' % (p1_desc, p2)
        print('%s;%s;%s' % (param, desc, '{:f}'.format(coef)))
    print('')


def predict():
    """Save all country predictions and store as inputs for the microsim."""
    # Countries listed in order of AES uptake (excluding LUX)
    sorted_countries = ['ROU', 'LTU', 'HRV', 'ELL', 'NED', 'ESP', 'DAN', 'MLT', 'ITA', 'LVA', 'FRA',
                 'POR', 'CYP','SVK', 'POL', 'IRE', 'BGR', 'SVN', 'HUN', 'DEU', 'BEL', 'CZE',
                 'UKI', 'EST', 'SVE', 'OST', 'SUO']
    for country in (sorted_countries):
        print(data_helper.names[country])
        year = results.data_used[country]
        vars = results.split_data[country]

        X_train, y_train = data_helper.get_data_even_split(country, year)
        X_train = X_train[vars].copy()
        y_train = np.array(y_train)

        model = sm.GLM(y_train, X_train, family=sm.families.Binomial()).fit()
        #print_brief_summary(model)

        X_test, y_test = data_helper.get_data(country, year, include_ID=True)
        IDs = X_test['ID']
        X_test = X_test[vars]
        y_pred = model.predict(X_test)

        #y_test = np.array(y_test)
        #plot_results_grid(y_test, y_pred)

        table = pd.DataFrame()
        table['ID'] = IDs
        table['mod'] = y_pred
        table['mod_rank'] = y_pred.argsort().argsort()
        table.to_csv('../microsim/inputs/mods/%s.csv' % country, index=False)


if __name__ == '__main__':
    predict()
