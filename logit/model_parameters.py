""" This file is used to generate and save the
    - opennness
    - subsets of mod ranks
    for each FSA and economic size subgroup to be used by the microsim.

The subsets of mod ranks takes the rank order of predictions provided by the
logit model, and works out the rank orders for each FSA and economic size group.
Note that this can take a short while to run.
"""

import csv
import numpy as np
import pandas as pd

import data_helper
import results


TYPE_OF_FARMING = 'TF8'
ECO_SIZE = 'SE005'
AES_PAYMENT = 'SAEAWSUB_V'

N_FSA_TYPES = 5
N_ECO_SIZE_CLUSTERS = 3
SMALL, MEDIUM, LARGE = range(N_ECO_SIZE_CLUSTERS)
THRESH_SMALL, THRESH_MEDIUM, THRESH_LARGE = (7000, 30000, 1000000)


with open('../data_fp', 'r') as f:
    data_fp = f.readline()
    try:
        # Remove any superfluous new line
        data_fp = data_fp[:data_fp.index('\n')]
    except ValueError:
        pass


def _fsa(row):
    """Get the Farm System Archetype of the farm."""
    TF = int(row['TF8'])
    if TF == 1:
        return 1  # general cropping
    elif TF == 2:
        return 2  # horticulture
    elif TF == 3 or TF == 4:
        return 3  # permanent crops
    elif 5 <= TF <= 7:
        return 4  # livestock
    return 5  # mixed


def _eco_size(row):
    """Get the clustered economic size of the farm."""
    farm_size = row[ECO_SIZE]
    if farm_size <= THRESH_SMALL:
        return SMALL
    elif farm_size <= THRESH_MEDIUM:
        return MEDIUM
    else:
        return LARGE


def save_opennness():
    """ For each FSA and each economic size find the percentage of people
        who adopted a scheme and set that as the openness of that group.
    """
    for country in data_helper.countries:
        year = data_helper.data_used[country]
        aes_counts = np.zeros((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
        farmer_counts = np.zeros((N_FSA_TYPES, N_ECO_SIZE_CLUSTERS))
        farmers = pd.read_csv('%s/%s%s.csv' % (data_fp, country, year),
                              usecols=(TYPE_OF_FARMING, ECO_SIZE, AES_PAYMENT, 'COUNTRY'))
        for index, row in farmers.iterrows():
            fsa = _fsa(row) - 1
            eco_size = _eco_size(row)
            if row[AES_PAYMENT] > 0:
                aes_counts[fsa][eco_size] += 1
            farmer_counts[fsa][eco_size] += 1
        # Save the percentage of farmers who take on a scheme in each group
        with open('../microsim/inputs/openness/%s.csv' % country, 'w') as csvfile:
            opennness_writer = csv.writer(csvfile)
            for eco_size in range(N_ECO_SIZE_CLUSTERS-1, -1, -1):
                for fsa in range(N_FSA_TYPES):
                    if farmer_counts[fsa][eco_size] > 0:
                        opennness_writer.writerow((aes_counts[fsa][eco_size] / farmer_counts[fsa][eco_size],))
                    else:
                        opennness_writer.writerow((0,))
        # Save the total number of farmers in each group (regardless of scheme uptake)
        with open('../microsim/inputs/total_farmers/%s.csv' % country, 'w') as csvfile:
            total_writer = csv.writer(csvfile)
            for eco_size in range(N_ECO_SIZE_CLUSTERS-1, -1, -1):
                for fsa in range(N_FSA_TYPES):
                    total_writer.writerow((farmer_counts[fsa][eco_size],))


def save_subset_mods():
    """ Convert the mod ranks obtained from the logit model
        to a seperate rank list for each FSA and economic size group.
    """
    codes = list(data_helper.countries)
    for country in codes:
        print('Processing country %s' % data_helper.names[country])
        year = results.data_used[country]
        farmers_df = pd.read_csv('%s/%s%s.csv' % (data_fp, country, year),
                              usecols=(TYPE_OF_FARMING, ECO_SIZE, AES_PAYMENT, 'COUNTRY', 'ID'))
        mods_df = pd.read_csv('../microsim/inputs/mods/%s.csv' % country)
        for eco_size in range(N_ECO_SIZE_CLUSTERS-1, -1, -1):
            for fsa in range(N_FSA_TYPES):
                ids = []
                mods = []
                for i in range(mods_df.shape[0]):
                    farm_fsa = _fsa(farmers_df.loc[i]) - 1
                    farm_eco_size = _eco_size(farmers_df.loc[i])
                    if farm_fsa == fsa and farm_eco_size == eco_size:
                        ids.append(farmers_df.loc[i]['ID'])
                        mods.append(mods_df.loc[i]['mod'])
                mod_rank = np.array(mods).argsort().argsort()
                mod_rank = mod_rank + 1
                for i in range(len(ids)):
                    mods_df.loc[mods_df['ID'] == ids[i], 'subset_mod_rank'] = mod_rank[i]
        mods_df.to_csv('../microsim/inputs/mods/%s.csv' % country, index=False)


if __name__ == '__main__':
    save_opennness()
    save_subset_mods()
